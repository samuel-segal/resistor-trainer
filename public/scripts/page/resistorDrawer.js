const canvas = document.getElementById('resistorCanvas');
const ctx = canvas.getContext('2d');

const canvasWidth = canvas.offsetWidth;
const canvasHeight = canvas.offsetHeight;

const resistorWidth = Math.min(canvasWidth,300);
const resistorHeight = Math.min(canvasHeight,80);

const numBands = 3;
const bandsWidth = resistorWidth/numBands * 0.05;

const drawResistor = function(resistor){

	ctx.fillStyle = "#e0d5bc";
	
	const dx = (canvasWidth-resistorWidth)/2;
	const dy = (canvasHeight-resistorHeight)/2;
	
	const startx = dx;
	const endx = canvasWidth - dx;
	const starty = dy;
	const endy = canvasHeight - dy;
	
	ctx.fillRect(startx,starty,endx-startx,endy-starty);

	let absoluteWidth = resistorWidth/numBands;
	for(let i=0;i<numBands;i++){
		const x = absoluteWidth*i/2 + dx + 5;

		const color = resistor.primaryBands[i];

		ctx.fillStyle = color;

		ctx.fillRect(x,starty,bandsWidth,endy-starty);
	}

}