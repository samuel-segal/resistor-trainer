class ResistorCache{

	constructor(){
		this.changeResistor();
	}

	getResistor(){
		return this.resistor;
	}

	changeResistor(){
		this.resistor = getRandomFourBand();
	}

}