const drawer = new ResistorCache();

const numGuesses = document.getElementById('correctCounter');
const resistanceBox = document.getElementById('resistanceBox');
const resistanceButton = document.getElementById('resistanceButton');

let correctGuesses = 0;

const onRight = function(){
	correctGuesses++;

	numGuesses.innerHTML = correctGuesses;

	drawer.changeResistor();
	const resist = drawer.getResistor();
	drawResistor(resist);
}

const onWrong = function(){

}

resistanceButton.onclick = function(){
	const text = resistanceBox.value;
	const num = (+text);

	const currResist = drawer.getResistor();
	console.log(currResist.resistance)
	const isCorrect = (num==currResist.resistance);

	//This can be made concise, but verbosity's better here
	if(isCorrect){
		onRight();
	}
	else{
		onWrong();
	}

	resistanceBox.value = '';
}

const res = drawer.getResistor();
drawResistor(res);