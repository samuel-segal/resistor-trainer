//Yes, I know this isn't the most efficienct way: I prefer it for the readability
const numsToColors = {
	0: 'black',
	1: 'brown',
	2: 'red',
	3: 'orange',
	4: 'yellow',
	5: 'green',
	6: 'blue',
	7: 'violet',
	8: 'grey',
	9: 'white'
}

//Values are expressed in percent
const toleranceToColors = {
	1: 'brown',
	2: 'red',
	0.5: 'green',
	0.25: 'blue',
	0.1: 'violet',
	5: 'gold',
	10: 'silver'
}

const colorsToNums = {
	'black': 0,
	'brown': 1,
	'red': 2,
	'orange': 3,
	'yellow': 4,
	'green': 5,
	'blue': 6,
	'violet': 7,
	'grey': 8,
	'white': 9
}



const getNumColor = function(num){
	if(num<0||num>9){
		throw new Error('Number out of bounds!')
	}
	
	return numsToColors[num]
}

const getToleranceColor = function(num){
	let out = toleranceToColors[num]

	if(!out){
		throw new Error(`${num} is not a valid tolerance!`)
	}

	return out
}

const getNum= function(color){
	let out = colorsToNums[color]

	if(!out){
		throw new Error(`${color} is not a valid color!`)
	}

	return out
}
