
//Currently only uses a 4 band resistor setup: may include more in the future
class Resistor {
	 
	static get defaultTolerance(){
		return 5
	}

	static get numNormalBands(){
		return 2
	}

	constructor(resistance,tolerancePercent){
		this.resistance = resistance
		this.primaryBands = Resistor.getColorArray(resistance)
		
		if(!tolerancePercent){
			tolerancePercent = Resistor.defaultTolerance
		}
		
		this.tolerancePercent = tolerancePercent
		this.toleranceBand = getToleranceColor(tolerancePercent)	
	}

	//The number of bands that represent a number directly, not a multiplier, tolerance or temperature coefficient


	static getColorArray(num){
		let numStr = num.toString()
		//Note: This may be buggy on single digit resistances
		let numDigits = numStr.length

		let firstTwo = numStr.substring(0,2)
		//Prefaces the value with 
		while(firstTwo.length<2){
			firstTwo = '0' + firstTwo
		}

		//Insures it can't be less than 0
		let tenPower = numDigits-Resistor.numNormalBands
		tenPower = Math.max(tenPower,0)

		let outArray = new Array()

		for(const char of firstTwo){
			let color = getNumColor(char)
			outArray.push(color)
		}
		
		let tenColor = getNumColor(tenPower)
		outArray.push(tenColor)

		return outArray
	}

	
}