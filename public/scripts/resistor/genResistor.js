const getRandomTolerance = function(){
	let keys = Object.keys(toleranceToColors)
	let randInd = Math.floor(Math.random()*keys.length)
	let tolerance = keys[randInd]

	return tolerance
}


const getRandomFourBand = function(){
	//Simple random range
	let base = Math.floor(Math.random()*100)
	let exp = Math.floor(Math.random()*10)
	let resistance = base * Math.pow(10,exp)
	let tolerance = getRandomTolerance()

	let resistor = new Resistor(resistance,tolerance)
	return resistor
}